import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainTopComponent } from './main-top/main-top.component';
import { MainBodyComponent } from './main-body/main-body.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { CartComponent } from './cart/cart.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { NavComponent } from './nav/nav.component';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';
import { SearchComponent } from './search/search.component';
import { MainBannerComponent } from './main-banner/main-banner.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BannerComponent } from './banner/banner.component';
import { CallToActionComponent } from './call-to-action/call-to-action.component';
import { FeaturedProductComponent } from './featured-product/featured-product.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    MainTopComponent,
    MainBodyComponent,
    HomeComponent,
    ProductsComponent,
    CartComponent,
    TopBarComponent,
    NavComponent,
    BottomBarComponent,
    SearchComponent,
    MainBannerComponent,
    NavbarComponent,
    BannerComponent,
    CallToActionComponent,
    FeaturedProductComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
